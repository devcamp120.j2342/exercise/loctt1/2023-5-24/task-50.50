public class Task5040 {
    public static void main(String[] args) throws Exception {
        String input = "DCresource: JavaScript Exercises";
        char n = 'e';
        
        int count = countCharacter(input, n);
        System.out.println(count);

          //Loại bỏ các ký tự trắng ở đầu và cuối chuỗi
          System.out.println("-----------------------");
            String input1 = " dcresource ";
            String input2 = " dcresource";
            String input3 = "dcresource ";

            String output1 = input1.trim();
            String output2 = input2.trim();
            String output3 = input3.trim();

            System.out.println(output1);
            System.out.println(output2);
            System.out.println(output3);
            
            
        System.out.println("-----------------------");
        String input4 = "The quick brown fox jumps over the lazy dog";
        String subString = "the";
        
        String result = removeSubstring(input4, subString);
        System.out.println(result);

        System.out.println("-----------------------");
        String str1 = "JS PHP PYTHON";
        String str2 = "PYTHON";
        String str3 = "JS";
        
        boolean isEndsWithStr2 = checkEndsWith(str1, str2);
        boolean isEndsWithStr3 = checkEndsWith(str1, str3);
        
        System.out.println(isEndsWithStr2); // true
        System.out.println(isEndsWithStr3); // false

        System.out.println("-----------------------");

        String str4 = "abcd";
        String str5 = "AbcD";

        boolean result2 = compareStrings(str4, str5);
        System.out.println(result2);

        System.out.println("-----------------------");
        String input6 = "Js STRING EXERCISES";
        int n1 = 1;
        int n2 = 2;
        
        System.out.println(isUpperCase(input6, n1)); // false
        System.out.println(isUpperCase(input6, n2)); // true

        System.out.println("-----------------------");
        String input7 = "Js STRING EXERCISES";
        int n4 = 1;
        int n5 = 2;
        
        System.out.println(isLowerCase(input7, n4)); // false
        System.out.println(isLowerCase(input7, n5)); // true

        System.out.println("-----------------------");
        String str7 = "js string exercises";
        String str8 = "js";
        
        boolean result4 = startsWith(str7, str8);
        System.out.println(result4);

        System.out.println("-----------------------");
        String input8 = "abc";
        
        boolean isEmpty = checkEmpty(input8);
        System.out.println(isEmpty);

        System.out.println("-----------------------");
        String input9 = "AaBbc";
        
        // Gọi phương thức reverseString() để đảo ngược chuỗi đã cho
        String reversedString = reverseString(input9);
        
        // In ra chuỗi đã đảo ngược
        System.out.println(reversedString);
    }

    //Đếm số lần xuất hiện của ký tự n trong chuỗi
    public static int countCharacter(String input, char n) {
        int count = 0;
        
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == n) {
                count++;
            }
        }
        return count;
    }

    //Loại bỏ chính xác chuỗi con ra khỏi chuỗi cho trước
    public static String removeSubstring(String input, String subString) {
        // Chuyển đổi cả chuỗi ban đầu và chuỗi con sang chữ thường để so sánh
        String lowercaseInput = input.toLowerCase();
        String lowercaseSubString = subString.toLowerCase();
        
        // Tìm vị trí đầu tiên của chuỗi con trong chuỗi ban đầu
        int index = lowercaseInput.indexOf(lowercaseSubString);
        while (index != -1) {
            // Xoá chuỗi con tại vị trí đã tìm thấy
            input = input.substring(0, index) + input.substring(index + subString.length());
            
            // Tiếp tục tìm vị trí tiếp theo của chuỗi con trong chuỗi ban đầu
            index = lowercaseInput.indexOf(lowercaseSubString, index + 1);
        }
        
        return input;
    }

    //Kiểm tra chuỗi sau có phải là kết thúc của chuỗi trước hay không
    public static boolean checkEndsWith(String fullString, String ending) {
        return fullString.endsWith(ending);
    }

    //So sánh 2 chuỗi có giống nhau hay không
    public static boolean compareStrings(String str1, String str2) {
        if (str1.equalsIgnoreCase(str2)) {
            return true;
        } else {
            return false;
        }
    }


    //Kiểm tra ký tự thứ n của chuỗi có phải viết hoa hay không
    public static boolean isUpperCase(String input, int n) {
        // Kiểm tra xem n có nằm trong khoảng hợp lệ của chuỗi không
        if (n < 1 || n > input.length()) {
            throw new IllegalArgumentException("Giá trị n không hợp lệ");
        }
        
        char character = input.charAt(n - 1); // Lấy ký tự tại vị trí n-1 trong chuỗi
        
        return Character.isUpperCase(character);
    }

    //Kiểm tra ký tự thứ n của chuỗi có phải viết thường hay không
    public static boolean isLowerCase(String input, int n) {
        if (n < 1 || n > input.length()) {
            throw new IllegalArgumentException("Giá trị n không hợp lệ");
        }
        
        char character = input.charAt(n - 1);
        
        return Character.isLowerCase(character);
    }

    //Kiểm tra chuỗi trước có bắt đầu bằng chuỗi sau hay không
    public static boolean startsWith(String str1, String str2) {
        return str1.startsWith(str2);
    }

    //Kiểm tra chuỗi đã cho có phải chuỗi rỗng hay không
    public static boolean checkEmpty(String input) {
        return input.isEmpty();
    }

    //Đảo ngược chuỗi
    public static String reverseString(String input) {
        // Sử dụng StringBuilder để hiệu quả hóa việc xây dựng chuỗi đã đảo ngược
        StringBuilder reversed = new StringBuilder();
        
        // Duyệt từ cuối chuỗi đến đầu chuỗi
        for (int i = input.length() - 1; i >= 0; i--) {
            // Lấy ký tự tại vị trí i và thêm vào StringBuilder
            reversed.append(input.charAt(i));
        }
        
        // Trả về chuỗi đã đảo ngược
        return reversed.toString();
    }
}
